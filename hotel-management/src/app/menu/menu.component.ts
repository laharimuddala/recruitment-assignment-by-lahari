import { Component, OnInit } from '@angular/core';
import { Menu } from '../menu';
import { MENU } from '../mock-starters';
import { MAIN } from '../mock-starters';
import { DESERTS } from '../mock-starters';
import { MONTH } from '../mock-starters';
import { DAILY } from '../mock-starters';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  menu = MENU;
  main = MAIN;
  deserts = DESERTS;
  month = MONTH;
  daily = DAILY;	

  constructor() { }

  ngOnInit() {
  }

};