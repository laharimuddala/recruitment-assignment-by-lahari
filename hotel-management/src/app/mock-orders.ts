import { Orders } from './menu';

export const ORDERS: Orders[] = [
  { tableno: 1, orderid: 100},
  { tableno: 2, orderid: 101},
  { tableno: 3, orderid: 102},
  { tableno: 4, orderid: 103},
  { tableno: 5, orderid: 104},
  { tableno: 6, orderid: 105},
  { tableno: 7, orderid: 106},
  { tableno: 8, orderid: 107},
  { tableno: 9, orderid: 108},
  { tableno: 10, orderid: 190}
];