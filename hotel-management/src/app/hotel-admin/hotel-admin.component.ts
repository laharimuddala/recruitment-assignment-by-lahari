import { Component, OnInit } from '@angular/core';
import { Menu } from '../menu';
import { MENU } from '../mock-starters';
import { MAIN } from '../mock-starters';
import { DESERTS } from '../mock-starters';

@Component({
  selector: 'app-hotel-admin',
  templateUrl: './hotel-admin.component.html',
  styleUrls: ['./hotel-admin.component.css']
})
export class HotelAdminComponent implements OnInit {

  menu = MENU;
  main = MAIN;
  deserts = DESERTS;
  selectedMenu: Menu;
	
  constructor() { }

  ngOnInit() {
  }
  onSelect(menu: Menu): void {
    this.selectedMenu = menu;
  }

}
