import { Menu } from './menu';

export const MENU: Menu[] = [
  { id: 11, name: 'chilli chicken' ,price: 100},
  { id: 12, name: 'chicken 65' ,price: 150},
  { id: 13, name: 'chicken manchuria' ,price: 200},
  { id: 14, name: 'chicken lollipop' ,price: 250},
  { id: 15, name: 'majestic chicken' ,price: 280},
  { id: 16, name: 'dragon chicken' ,price: 300},
  { id: 17, name: 'chicken spring roll' ,price: 320},
  { id: 18, name: 'chicken 555' ,price: 380},
  { id: 19, name: 'chicken nuggets' ,price: 400},
  { id: 20, name: 'chicken factory' ,price: 440}
];
export const MAIN: Menu[] = [
  { id: 21, name: 'chicken biryani' ,price: 100},
  { id: 22, name: 'sp. chicken biryani' ,price: 150},
  { id: 23, name: 'pot biryani' ,price: 200},
  { id: 24, name: 'leg peice biryani' ,price: 250},
  { id: 25, name: 'mutton biryani' ,price: 280},
  { id: 26, name: 'shahi chicken biryani' ,price: 300},
  { id: 27, name: 'chicken heaven biryani' ,price: 320},
  { id: 28, name: 'mixed biryani' ,price: 380},
  { id: 29, name: 'veg biryani' ,price: 400},
  { id: 30, name: 'curd rice' ,price: 440}
];
export const DESERTS: Menu[] = [
  { id: 31, name: 'abc' ,price: 100},
  { id: 32, name: 'def' ,price: 150},
  { id: 33, name: 'Bom' ,price: 200},
];
export const MONTH:Menu[]=[
  { id:34, name: 'mutton haleem' ,price:250},
  { id:35, name: 'chicken haleem' , price:300},
];
export const DAILY:Menu[]=[
  { id:36, name: 'appolo fish' ,price:250},
];