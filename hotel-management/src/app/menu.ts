export class Menu {
  id: number;
  name: string;
  price: number;
}
export class Orders{
  tableno:number;
  orderid:number;
}
export class Items{
	orderid:number;
	item:string;
	quantity:number;
}